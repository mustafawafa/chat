
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import io from 'socket.io-client';
// window.socket = io('127.0.0.1:3000');

Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// window.app = new Vue({
//     el: '#app',
//     data : {
//         messages : []
//     },
//     mounted(){
//
//
//         socket.on('chat',function(message){
//             this.messages.push(message)
//         }.bind(this));
//
//
//         //
//         // Echo.channel('test')
//         //     .listen('TestEvent', (e) => {
//         //         this.messages.push(e)
//         //     });
//     }
// });



