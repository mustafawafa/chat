@extends('layouts.app')

@section('content')
    <div class="container">
            <div class="container">
                <form action="rooms" method="post">
                    {{--<div class="form-group row">--}}
                        {{--<label for="roomName" class="col-sm-2 col-form-label">Name</label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input name="roomName" type="text" class="form-control" id="roomName" placeholder="Room Nme:">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="roomName" class="col-sm-2 col-form-label">Name:</label>
                        <div class="col-sm-10">
                            <input name="roomName" type="text" class="form-control" id="roomName" placeholder="Name of Room:">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="roomNumber" class="col-sm-2 col-form-label">Number of people:</label>
                        <div class="col-sm-10">
                            <input name="roomNumber" type="number" class="form-control" id="roomNumber" placeholder="Number of people">
                        </div>
                    </div>
                    <fieldset class="form-group row">
                        <legend class="col-form-legend col-sm-2">Security</legend>
                        <div class="col-sm-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input name="roomType" class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="open" checked>
                                        Open for All
                                    <label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input  name="roomType" class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="private">
                                    Private
                                </label>
                            </div>

                        </div>
                    </fieldset>

                    <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">create</button>
                        </div>
                    </div>
                </form>
            </div>


    </div>


@endsection



@section('scripts')



@endsection



