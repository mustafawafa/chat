
@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row" id='home'>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h4> Welcome to Slim Chatty </h4>
                        <form action="rooms" method="post">
                            {{csrf_field()}}
                            <a href="rooms" class="btn btn-danger">Create</a>

                        </form>
                        <br>
                        <h1>Current Rooms:</h1>
                        <ul class="list-group">

                            @foreach($currentRooms as $roomID => $room)
                                <li class="list-group-item">

                                    <span class="badge">{{$room['availablePlaces']}}</span>
                                    <a href="/room/{{$roomID}}">{{$room['name']}}</a>

                                </li>
                            @endforeach
                        </ul>
                        <br>
                        <h1>Avaialbe Rooms:</h1>

                        <ul class="list-group">

                        @foreach($availableRooms as $roomID => $room)
                                <li class="list-group-item">

                                <span class="badge">{{$room['availablePlaces']}}</span>
                                <a href="/room/{{$roomID}}">{{$room['name']}}</a>

                                </li>
                        @endforeach
                            <li v-for="room in rooms" class="list-group-item">
                                <span class="badge">@{{room.roomDetails.availablePlaces}}</span>
                                <a href="/room/  ">@{{room.roomDetails.name}}</a>

                            </li>
                        </ul>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')

    <script>
        var app = new Vue({

            el : '#app',

            data : {
                rooms : []

            },
            mounted(){
                Echo.private('newOpenRooms')
                    .listen('OpenRoomCreated',(room)=>{
                        this.rooms.push(room);
                    });
            }

        });

    </script>



    @endsection