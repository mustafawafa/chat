@extends('layouts.app')

@section('content')
        <div id="chat">

            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <h1>{{$name}}</h1>
                            @foreach($messages as $message)
                            
                                <div>
                                    <span><a href="#">{{ $message->user->name }} sends:</a></span>
                                    @if($message->type === 'txt')
                                    <div > {{ $message->body }}</div>

                                    @elseif($message->type === 'img')

                                    <div><a href='{{$message->body}}'> <img class="img" src="{{$message->body}}"  /></a></div>
                                    @else

                                    <div><a href='{{$message->body}}'> {{ basename($message->body) }}</a></div>
                                    @endif
                             </div>
                            @endforeach
                            <div v-for="message in messages">
                                <span><a href="#">@{{ message.userName }} sends:</a></span>
                                <div  v-html='message.message'></div>
                            </div>
                        </ul>

                        <div class="input-group fixed-input">
                        <input  @change='upload' type="file" id="imgupload" style="display:none" ref='fileInput'/> 
                       

                       
                <textarea class="form-control message-textarea"  v-model="newMessage" name="" id="" cols="30" rows="10" placeholder="Enter Your Message"></textarea>
                  <button @click="sendMessage" class="btn btn-danger" >Send Message</button>
                  <button @click="openUpload" class="btn btn-danger" >upload file</button>
                        </div>

                    </div>

                    <div class="col-sm-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Users In this Room </div>
                            <div class="panel-body">
                                @foreach($users as $user)

                                    <h5><a href="#">{{$user->name}}</a></h5>
                                    <h5 v-for="user in users"><a href="#">@{{user.name}}   </a></h5>

                                @endforeach


                            </div>
                        </div>
                    </div>

                </div>





        </div>

    </div>


@endsection



@section('scripts')
    <script>
        new Vue({
            el : '#chat',
            data : {
                messages : [],
                newMessage : '',
                users : [],
                file: ''
            },
            mounted(){
//                socket.on('private-chat', function(message){
//                    this.messages.push(message);
//                }.bind(this));
                Echo.private('chat-{{$id}}')
              .listen('MessageSent', (e) => {
                  console.log(e.type);
                  if(e.type === 'txt'){
                      e.message = e.message
                  }
                  else if(e.type === 'img'){
                      e.message = "<a href='" +e.message +'\'> <img class="img" src="' + e.message + '" /></a>'
                  }
                  else{
                      e.message = "<a href='" +e.message +"'>" + e.name +" </a>"
                  }
                  this.messages.push(e)
              });

                Echo.private('newUserIn{{$id}}')
                    .listen('UserEnteredChat',(user)=>{

                        this.users.push(user.user);
                    });

            },
            methods : {
                sendMessage(e){
                    axios.post('/messages',{

                        message : this.newMessage,
                        room : '{{$id}}'

                    }).then(function(){
                        this.newMessage = '';
                        console.log('done');
                    }.bind(this))
                        .catch(function (error) {
                            console.log(error);
                        });

                },


                openUpload(e){
                    this.$refs.fileInput.click()

                },
                upload(){
                    var files = new FormData();
                    files.append('file',document.getElementById('imgupload').files[0]);
                    files.append('room','{{$id}}');
                    
                    axios.post('/test',files).then(function(e){
                        console.log(e);
                    });
                }
        }

        })
    </script>


@endsection