<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public  $room;
    public $userName;
    public $userID;
    public $type;
    public $name;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message,$room,$type,$name = null)
    {
        $this->message = $message;
        $this->room = $room;
        $this->userName = auth()->user()->name;
        $this->userID = auth()->user()->id;
        $this->type = $type;
        $this->name = $name;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat-' . $this->room);
//        return new Channel('chat');
    }
}
