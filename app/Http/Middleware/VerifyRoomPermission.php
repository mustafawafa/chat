<?php

namespace App\Http\Middleware;

use Closure;
use Redis;
class VerifyRoomPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //if open or closed
        $room = unserialize(Redis::hget('rooms', request('id')));
        
        if(( $room['type'] =="open"  && $room['availablePlaces'] !== 0) || in_array(auth()->id(),$room['users'])) {
            return $next($request);
        }
//        dd($pass);
        return redirect('home');
    }
}
