<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redis;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Redis::hgetall('rooms');

        //un
        $rooms = array_map('unserialize',$rooms);

        $currentRooms = array_filter($rooms,array($this,'checkCurrentRooms'));
        $availableRooms = array_filter($rooms,array($this,'checkAvailableRooms'));


        return view('home',compact('currentRooms','availableRooms'));
    }

    protected function checkAvailableRooms($room){

        if($room['type'] === 'open' && $room['availablePlaces'] > 0  && ! in_array(auth()->id() , $room['users'] ) ){
            return true;
        }
    }


    protected function checkCurrentRooms($room){
        if(in_array(auth()->id() , $room['users'] )){
            return true;
        }

    }
}
