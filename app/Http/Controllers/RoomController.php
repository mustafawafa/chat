<?php

namespace App\Http\Controllers;

use App\Events\OpenRoomCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Message;
use App\Events\UserEnteredChat;
use App\User;

class RoomController extends Controller
{

        public function __construct()
        {
            $this->middleware('auth');
        }

    public function create(){

            $room = serialize([
                'name' => request('roomName'),
                'number' => request('roomNumber'),
                'type' => request('roomType'),
                'availablePlaces' => request('roomNumber'),
                'users' => []
            ]);

           $roomId= 'room' . uniqid();

            Redis::hset('rooms',$roomId , $room);

            OpenRoomCreated::dispatch($roomId,unserialize($room));

            return redirect("/room/{$roomId}");
    }


    public function newRoom(){

            return view('newRoom');

    }

    public function accessRoom($id){

    $messages = Message::where('room',$id)->with('user')->get();

    $oldRoom = unserialize(Redis::hget('rooms', $id));
    $name = $oldRoom['name'];

    $availablePlaces =  $oldRoom['availablePlaces'] ;
    
        if( ! in_array(auth()->id(), $oldRoom['users'] )){ // Enter Room for the first time.

            UserEnteredChat::dispatch(auth()->user(),$id);
            $availablePlaces-- ;
    
            $oldRoom['users'][] = auth()->id();
    
        }
        $room = serialize([
            'name' => $oldRoom['name'],
            'number' => $oldRoom['number'],
            'type' => $oldRoom['type'],
            'availablePlaces' => $availablePlaces,
            'users' => $oldRoom['users']
        ]);
    
        $users = User::whereIn('id',$oldRoom['users'])->get();

        Redis::hset('rooms',$id,$room);
    
        return view('chat',compact('id','name','users','messages'));
    

    }
}
