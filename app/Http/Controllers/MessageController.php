<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        Message::create([
            'user_id' => auth()->id(),
            'room' => request('room'),
            'body' => request('message'),
            'type' => 'txt'
        ]);

        event(new MessageSent(request('message'),request('room'),'txt'));

    }

    public function fileUpload(){
        request()->validate([
                'file' => 'mimetypes:image/jpeg,image/png,application/pdf,video/webm,video/ogg,image/gif'

        ]);
        $path = request('file')->store('avatares','public');
$name = basename($path);
;
$url = Storage::disk('public')->url($path);
$type = Storage::disk('public')->mimeType($path);

$imagetypes = ['image/png','image/jpeg'];
if(in_array($type,$imagetypes)){
$type = 'img';
$name = null;
}
Message::create([
    'user_id' => auth()->id(),
    'room' => request('room'),
    'body' => ($url),
    'type' => $type ? $type : 'file'
]);
 MessageSent::dispatch($url,request('room'),$type,$name);
return;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
