<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
      'user_id','room','body','type'
    ];



    public function user(){

        return $this->belongsTo(User::class);
    }
}
