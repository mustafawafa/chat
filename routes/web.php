<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Events\MessageSent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/rooms','RoomController@newRoom');
Route::post('/rooms','RoomController@create');

Route::post('/messages','MessageController@create');

Route::get('room/{id}/{pass?}','RoomController@accessRoom')->middleware('room');


Route::get('users/{user}',"UserController@show");

Route::post('/test','MessageController@fileUpload');

Route::get('/test',function(){
    $path = storage_path() . '/app/avatares/7MIMH9Xt7SitA7KvRYireOfAmS8oXpyNC8u1NbGY.png';
    $file = File::get($path);
    
    MessageSent::dispatch($file);
});